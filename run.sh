#!/bin/bash

for f in *_spec.sh ; do
  echo $f
  time shellspec --require spec_helper $f >& /dev/null
  echo '-------------'
done
